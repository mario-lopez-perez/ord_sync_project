This folder contains the code to reproduce the collective analysis of stocks carried out in sections 5-7 of the paper. The file you have to compile to do this is main_collective.py; it is structured to follow the steps as indicated in the paper. There are three folders here: functions, output_csv and output_jpg.

The folder functions contains two folders: the first is called data_functions and contains the scripts with those functions used to process the data (calculate transcript entropy, degree and eigenvector centrality, clustering, etc.); the second is called plot_functions and contains the python scripts with the functions used to plot that data as in the paper (plot dendogram, similarity matrix, states dynamics, and so on).

The folder output_csv contains the data processed by some functions in data_functions, data which is saved in csv files in order to avoid repetition of heavy calculations, such as transcript synchronization dynamical network. In this way, once that data is at hand, the line in main_collective.py which carried out those calculations should be commented and the data accesed through the csv file with read_csv from pandas, as in main_collective.py.

The folder output_jpg contains the plots produced by the functions in plot_functions. The names of those jpg files are set to be self-explanatory (as long as you read the paper and the code in main_collective.py), as they carry the information about parameters used and content.

Please note that the figures are not exactly the same as in the article, because at that stage this code was not structured and the random seeds used to generate noise when calculating transcript entropy were not standardized. Nevertheless, the results are notably similar, which is not surprise as ordinal patterns are known to be robust to noise and even noise-enhaced. The conclusions of the paper remain thus unchanged.

The figures and csv files are already available in this repository and have been generated by compiling main_collective.py exactly as it is.

