from matplotlib import pyplot as plt
from scipy.cluster import hierarchy

def plot_dendogram(Z, t, matrix_type, phase_space, coph_coeff, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, metric = 'JS', nbins = 10):  #This function plots the dendogram Z calculated in dendogram.py, indicating threshold t with an horizontal line and displaying the cophenetic coefficient.
    fig, axes = plt.subplots(1, 1, figsize=(24, 24))
    dend = hierarchy.dendrogram(Z)#, p = 20, truncate_mode = 'level')
    axes.axhline(y = t, linewidth = 2, color='gray', linestyle = '-')
    axes.text(0.7, 0.7, 'Cophenetic Correlation: {}'.format("%.3f" % coph_coeff), horizontalalignment='center', verticalalignment='center', transform=axes.transAxes, fontsize = 'large', bbox=dict(boxstyle="round", facecolor='blue', alpha=0.2))
    plt.tight_layout(pad = 1)
    plt.savefig('collective_analysis/output_jpg/' + matrix_type +'_dendogram_sec2av_{}_N_{}_d{}_tau{}_noise{}_metric{}_{}_hist{}.jpg'.format(sec2av, N, dx, taux, noise_ampl, metric, phase_space, nbins), dpi = 300)
    plt.close()
