from matplotlib import pyplot as plt
import matplotlib
import copy
import numpy as np
from matplotlib.cm import ScalarMappable

def plot_3Dhist_bars(phase_rep, phase_val, states_noise, states, n_clusters, phase_space, algorithm, matrix_type = 'transcripts_PE', markersize = 0.3, sec2av = 1, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, metric = 'JS', nbins = 10): #This function plots the sequence of states and noise as defined by states_noise, output of dendogram or kmeans (see dendogram.py and kmeans_calculate.py), in the form of histograms colored after the state they belong to. It is saved in output_jpg. the parameter algorithm can be 'kmeans' or 'dendogram', and indicates which algorithm was used to obtain the states dynamics.
    if algorithm == 'kmeans':
        states_noise = states #kmeans will not label any point as noise.
    n = phase_rep.shape[0]
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.set_box_aspect(aspect = (1,2,1))

    cmap = copy.copy(matplotlib.cm.get_cmap("viridis", n_clusters))
    cmap.set_bad(color='white')
    
    for t in np.arange(n):
        xs = phase_val
        ys = phase_rep[t, :]
        if states_noise[t] != -1:
            cs = cmap(states_noise[t]/n_clusters)
            ax.bar(xs, ys, zs=t, zdir='y', color=cs, alpha=0.8, width = phase_val[1] - phase_val[0])
   
    if phase_space == 'Deg': 
        ax.set_xlabel('Degree')
    elif phase_space == 'EVC':
        ax.set_xlabel('EVC')

    ax.set_title('(a)', x = 0.1, y = 0.85, fontsize = 10)               

    ax.set_xticks(phase_val[: : 2])
    ax.set_xticklabels(["%.2f" % val for val in phase_val[: : 2]], fontsize = 'small') 

    ax.set_ylabel('Time')
    ax.set_zticklabels([])

    sm = ScalarMappable(cmap=cmap, norm=plt.Normalize(0,n_clusters-1))
    sm.set_array([])

    cbar = plt.colorbar(sm, ticks = (np.arange(n_clusters) + 0.5)*(n_clusters - 1)/n_clusters, orientation = "vertical", pad=0.06)
    cbar.ax.set_yticklabels([str(s) for s in range(n_clusters)])    
    
    cbar.set_label('State', rotation=270)#,labelpad=25)
    ax.view_init(20, 30) #(elev, azim)
    
    plt.subplots_adjust( wspace=0.40)    
   
    plt.savefig('collective_analysis/output_jpg/' + matrix_type + '_' + algorithm +'_3dhistogram_bars_States{}_sec2av_{}_N_{}_d{}_tau{}_noise{}_{}_hist{}.jpg'.format(n_clusters, sec2av, N, dx, taux, noise_ampl, phase_space, nbins), bbox_inches = 'tight', dpi = 300)   
   
    plt.close()
