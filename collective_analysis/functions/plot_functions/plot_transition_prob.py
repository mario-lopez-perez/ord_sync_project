import copy
import matplotlib
from networkx import algorithms
import numpy as np
from matplotlib import pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout
import networkx as nx

def transition_prob(states, n_clusters, phase_space, matrix_type, algorithm, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, nbins = 10): #This function plots the transition probabilities defined by the states dynamics, encoded in states, where states is an output of dendogram (see dendogram.py) or kmeans (see kmeans.py), and n_clusters is the number of clusters given by dendogram.     
    prob_transition = np.zeros((n_clusters, n_clusters))
    prob_state = np.zeros(n_clusters)
    lag = 1
    for i1 in range(n_clusters):
        prob_state[i1] = np.count_nonzero([states[t] == i1 for t in range(len(states))])
        for i2 in range(n_clusters):
            if np.count_nonzero([states[t] == i1 for t in range(len(states)-lag)]) == 0:
                prob_transition[i1, i2] = 0
            else:    
                prob_transition[i1, i2] = np.count_nonzero([states[t] == i1 and states[t+lag] == i2 for t in range(len(states)-lag)]) / np.count_nonzero([states[t] == i1 for t in range(len(states)-lag)])
    

    fig, axes = plt.subplots(1,2, figsize = (8, 4))
    ax1 = axes[0]       
    current_cmap = copy.copy(matplotlib.cm.get_cmap("coolwarm"))
    current_cmap.set_bad(color='white')
    ax1.set_xticks(np.arange(n_clusters))
    ax1.set_xticklabels([str(val) for val in np.arange(n_clusters)], rotation = 90)
    ax1.set_xlabel('State')
    ax1.set_yticks(np.arange(n_clusters)) 
    ax1.set_ylabel('State')
    ax1.set_title('(a)', x = 0.1, y = 0.85, fontsize = 10)                
    
    im = ax1.imshow(prob_transition, interpolation = 'none', cmap = 'coolwarm')#, vmin=0, vmax=1)

    cbar = plt.colorbar(im, fraction = 0.047, ax = [ax1])
    cbar.set_label('Transition Probability')  

   
#    plt.close()
 
 
    nodes = np.arange(n_clusters)#['s{}'.format(s) for s in states]
    edges = []
    weights = []
    for s1 in range(n_clusters):
        for s2 in range(n_clusters):
            if prob_transition[s1, s2] > 1/n_clusters:
                edges.append((s1, s2))  
                weights.append(prob_transition[s1, s2])
    weighted_edges = np.append(np.array(edges), np.array(weights).reshape(-1,1) , axis = 1)
    G = nx.MultiDiGraph()
    G.add_weighted_edges_from(weighted_edges, weight='weight')
    weights = np.array([G.edges[E]['weight'] for E in G.edges]).flatten()
    node_labels = {nodes[i] : 's{}'.format(i) for i in range(len(nodes))}    
        
    ax2 = axes[1]       
    ax2.set_title('(b)', x = 0.1, y = 0.85, fontsize = 10)                
    pos = graphviz_layout(G, prog='fdp')
    nx.draw_networkx_nodes(G, pos = pos, node_size = 20*prob_state)#, alpha=0.7, arrows=True)
    nx.draw_networkx_edges(G, pos = pos, width = 5*weights, arrows=True, connectionstyle="arc3,rad=0.3")     
    nx.draw_networkx_labels(G, pos, node_labels)#, font_size=22, font_color="whitesmoke")
    ax = plt.gca()

    plt.axis('off')

#    plt.tight_layout(pad = 1)
    plt.savefig('collective_analysis/output_jpg/' + matrix_type + '_' + algorithm + '_transition_network_States{}_sec2av_{}_N_{}_d{}_tau{}_noise{}_{}_hist{}.jpg'.format(n_clusters, sec2av, N, dx, taux, noise_ampl, phase_space, nbins), dpi = 300)   
   
    plt.close()
    
    return prob_transition, prob_state
