import numpy as np
from matplotlib import pyplot as plt
from functions.data_functions.dynamical_histogram import *
import copy
import matplotlib
from matplotlib.cm import ScalarMappable

def plot_states_sequence_dendogram(phase_rep, phase_val, states_noise, states, n_clusters, phase_space, matrix_type = 'transcripts_PE', markersize = 0.3, sec2av = 1, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, metric = 'JS', nbins = 10): #This function plots the sequence of states and noise as defined by states_noise, output of dendogram (see dendogram.py), as well as the ordered centroids of the clusters-states. It is saved in output_jpg.
    mean_states = np.zeros((n_clusters, phase_rep.shape[1])) #This array will contain the centrois of clusters
    for j in range(n_clusters):
        ind_j = np.argwhere(states_noise == j).flatten()
        mean_states[j, :] = np.nanmean(phase_rep[ind_j, :] , axis = 0)
    
    colorlist = ['crimson' if d in [81, 180, 200]  else 'black' if states_noise[d] == -1 else 'blue' for d in range(len(states_noise))] 
    markerlist = [80*markersize if d in [81, 180, 200]  else 2*markersize if states_noise[d] == -1 else 20*markersize for d in range(len(states_noise))] #Noise points are plotted as small black dots; non-noise points are as medium-sized blue circles; outlier points as big red dots.

    ylabel_list = ['Noise'] + list(np.unique(states))  
              
    fig, axs = plt.subplots(ncols=n_clusters, nrows=2, figsize = (3*n_clusters, 3*2) )
    for ax in axs[0, :]:
        ax.remove()

    gs = axs[1,1].get_gridspec()    

    axbig = fig.add_subplot(gs[0, :]) #This will be the subplot of the evolution of states dynamics; the other subplots are the ordered centroids.
    axbig.scatter(np.arange(len(states_noise)), states_noise, s = markerlist, c = colorlist)#, label = 'States-{}'.format(n_clusters))  
    axbig.set_xlabel(r'Time')
    axbig.set_ylabel(r'State')
    axbig.set_yticks(np.arange(-1, n_clusters))
    axbig.set_yticklabels(ylabel_list)
    axbig.set_title('(a)', x = 0.01, y = 0.85, fontsize = 10)    

    for j in range(n_clusters):
        if phase_space == 'Mix':
            axs[1, j].bar(phase_val[:nbins], mean_states[j, :nbins], width = phase_val[1]-phase_val[0], color = 'blue', alpha = 0.5)
            axs[1, j].bar(phase_val[nbins:], mean_states[j, nbins:], width = phase_val[nbins + 1]-phase_val[nbins], color = 'red', alpha = 0.5)
        elif phase_space == 'Deg':            
            axs[1, j].bar(phase_val, mean_states[j, :], width = phase_val[1] - phase_val[0], color = 'blue', alpha = 0.5)
            #axs[1, j].set_ylim((0,1))
        elif phase_space == 'EVC':            
            axs[1, j].bar(phase_val, mean_states[j, :], width = phase_val[1] - phase_val[0], color = 'red', alpha = 0.5)
        axs[1, j].set_xlabel(phase_space + '\n State {}'.format(j))
        axs[1, j].set_xticks(phase_val)
        axs[1, j].set_xticklabels(["%.2f" % val for val in phase_val], rotation = 90)
        axs[1, j].tick_params(axis = 'x', which = 'major', labelsize = 5)
        axs[1, j].set_ylim((0, 1.05*np.nanmax(mean_states)))        
        axs[1, j].set_yticks(np.arange(0, np.nanmax(mean_states), 5))                
    axs[1, 0].set_title('(b)', x = 0.1, y = 0.85, fontsize = 10)            

    plt.tight_layout(pad = 1)  
    
    plt.savefig('collective_analysis/output_jpg/' + matrix_type +'_Dendogram_States{}_sec2av_{}_N_{}_d{}_tau{}_noise{}_metric{}_{}_hist{}.jpg'.format(n_clusters, sec2av, N, dx, taux, noise_ampl, metric, phase_space, nbins), dpi = 300)
    plt.close()

