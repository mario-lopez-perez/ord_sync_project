import copy
from matplotlib import pyplot as plt
import matplotlib
import numpy as np

def plot_deg_EVC(Degree, Eigen_centrality, Labels, matrix_type, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001): #Plots the evolution of Degree and EVC vectors as functions of stocks through the year.
    ax_degree = plt.subplot(121)
    current_cmap = copy.copy(matplotlib.cm.get_cmap("coolwarm"))
    current_cmap.set_bad(color='white')
    im_deg = plt.imshow(Degree, interpolation = 'none', cmap = 'coolwarm')
    ax_degree.set_xlabel('Stock')
    ax_degree.set_xticks(np.arange(len(Labels)))
    ax_degree.set_xticklabels(Labels, rotation = 90)
    ax_degree.tick_params(axis = 'x', which = 'major', labelsize = 5)
    ax_degree.set_ylabel('Time')
    ax_degree.set_aspect('auto')
    ax_degree.set_title('(a)', x = 0.1, y = 0.85, fontsize = 10)
    cbar = plt.colorbar(im_deg)
    cbar.set_label('Degree')  
           
 
    ax_eigen = plt.subplot(122)
    current_cmap = copy.copy(matplotlib.cm.get_cmap("coolwarm"))
    current_cmap.set_bad(color='white')
    im_eig = plt.imshow(Eigen_centrality, interpolation = 'none', cmap = 'coolwarm')
    ax_eigen.set_xlabel('Stock')
    ax_eigen.set_xticks(np.arange(len(Labels)))
    ax_eigen.set_xticklabels(Labels, rotation = 90)
    ax_eigen.tick_params(axis = 'x', which = 'major', labelsize = 5)
    ax_eigen.set_ylabel('Time')
    ax_eigen.set_aspect('auto')
    ax_eigen.set_title('(b)', x = 0.1, y = 0.85, fontsize = 10)   
    cbar = plt.colorbar(im_eig)
    cbar.set_label('Eigenvector Centrality')  
 
    plt.subplots_adjust( wspace=0.6)#, hspace = 0.05)         
    plt.tight_layout(pad = 1)
     
    plt.savefig('collective_analysis/output_jpg/' + matrix_type + '_degree_eigen_sec2av_{}_N_{}_d{}_tau{}_noise{}.jpg'.format(sec2av, N, dx, taux, noise_ampl), dpi = 300)
    plt.close()          