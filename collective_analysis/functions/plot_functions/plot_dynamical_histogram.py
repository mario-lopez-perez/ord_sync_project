
import copy
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.cm import ScalarMappable
import numpy as np
from matplotlib import cm

def plot_3Dhist_surface(array_hist, array_values, phase_space, matrix_type, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, nbins = 10): #Plots the evolution of histograms of Degree or EVC, or any other array with the corresponfing changes.
    array_hist_max = np.nanmax(array_hist)
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.set_box_aspect(aspect = (1,2,1))
    time_ax = np.arange(array_hist.shape[0])
    values_ax, time_ax = np.meshgrid(np.arange(len(array_values)), time_ax)

    def f(v, t):
        return array_hist[t, v]
 
    array_hist = f(values_ax, time_ax)

    im = ax.plot_surface(values_ax, time_ax, array_hist, cmap=cm.coolwarm,
                       linewidth=0, antialiased=True)
    cmap = copy.copy(matplotlib.cm.get_cmap("coolwarm"))
    cmap.set_bad(color='white')
    ax.set_ylabel('Time')
    ax.set_xticks(np.arange(0, len(array_values), 2))
    ax.set_xticklabels(["%.2f" % val for val in array_values[: : 2]], fontsize = 'small')
#    ax.set_zlabel('Frequency')
    ax.set_zticklabels([])

    if phase_space == 'Deg':
        ax.set_xlabel('Degree')
        ax.set_title('(a)', x = 0.1, y = 0.85, fontsize = 10)
    elif phase_space == 'EVC':
        ax.set_xlabel('EVC')        
        ax.set_title('(b)', x = 0.1, y = 0.85, fontsize = 10)
        
    sm = ScalarMappable(cmap=cmap, norm=plt.Normalize(0, array_hist_max))
    sm.set_array([])

    cbar = plt.colorbar(sm, orientation = "vertical", pad=0.05)
#    cbar.ax.set_yticklabels([str(s) for s in range(n_clusters)])    
    cbar.set_label('Frequency', rotation=270, labelpad=25)
#    ax_degree.set_title('Dynamical Histogram \n of Degree') 
    ax.view_init(20, 30) #(elev, azim)

#    plt.show()
    plt.savefig('collective_analysis/output_jpg/' + matrix_type + '_3dhistogram_surface_sec2av_{}_N_{}_d{}_tau{}_noise{}_{}_hist{}.jpg'.format(sec2av, N, dx, taux, noise_ampl, phase_space, nbins), bbox_inches = 'tight', dpi = 300)   
   
    plt.close()



def plot_measures_deg_EVC(Degree, Degree_hist, Eigen_centrality, Eigen_hist, matrix_type, markersize = 0.3, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001):
    Degree_mean = np.nanmean(Degree, axis = 1)
    Degree_std = np.nanstd(Degree, axis = 1)
    Degree_min = np.nanmin(Degree, axis = 1)
    Degree_hist_max = np.nanmax(Degree_hist, axis = 1)
 
    Eigen_mean = np.nanmean(Eigen_centrality, axis = 1)
    Eigen_std = np.nanstd(Eigen_centrality, axis = 1)
    Eigen_min = np.nanmin(Eigen_centrality, axis = 1)
    Eigen_hist_max = np.nanmax(Eigen_hist, axis = 1)

    plt.subplots(2,2)
    plt.title('Degree')

    plt.subplot(2,2,1)
    plt.scatter(np.arange(len(Degree_mean)), Degree_mean, s = 3*markersize, label = 'Mean')  
    plt.xlabel(r'Time')
    plt.ylabel(r'Degree Mean')
    plt.title('(a)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')

    plt.subplot(2,2,2)
    plt.scatter(np.arange(len(Degree_std)), Degree_std, s = 3*markersize, label = 'Std')  
    plt.xlabel(r'Time')
    plt.ylabel(r'Degree Standard Deviation')
    plt.title('(b)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')

    plt.subplot(2,2,3)
    plt.scatter(np.arange(len(Degree_min)), Degree_min, s = 3*markersize, label = 'Min')  
    plt.xlabel(r'Time')
    plt.ylabel(r'Degree Minimum')
    plt.title('(c)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')

    plt.subplot(2,2,4)
    plt.scatter(np.arange(len(Degree_hist_max)), Degree_hist_max, s = 3*markersize, label = 'MaxFreq')  
    plt.xlabel(r'Time')
    plt.ylabel(r'Degree Max Frequency')
    plt.title('(d)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')
    plt.subplots_adjust( wspace=0.40)    

    plt.tight_layout(pad = 1)
    plt.savefig('collective_analysis/output_jpg/'  + matrix_type + '_Degree_sec2av_{}_N_{}_d{}_tau{}_noise{}.jpg'.format(sec2av, N, dx, taux, noise_ampl), dpi = 300)
    plt.close()

    plt.subplots(2,2)
    plt.title('Eigen')

    plt.subplot(2,2,1)
    plt.scatter(np.arange(len(Eigen_mean)), Eigen_mean, s = 3*markersize, label = 'Mean')  
    plt.xlabel(r'Time')
    plt.ylabel(r'EVC Mean')
    plt.title('(a)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')

    plt.subplot(2,2,2)
    plt.scatter(np.arange(len(Eigen_std)), Eigen_std, s = 3*markersize, label = 'Std')  
    plt.xlabel(r'Time')
    plt.ylabel(r'EVC Standard Deviation')
    plt.title('(b)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')

    plt.subplot(2,2,3)
    plt.scatter(np.arange(len(Eigen_min)), Eigen_min, s = 3*markersize, label = 'Min')  
    plt.xlabel(r'Time')
    plt.ylabel(r'EVC Minimum')
    plt.title('(c)', x = 0.1, y = 0.85, fontsize = 10)
    #plt.legend(fontsize = 'xx-large')

    plt.subplot(2,2,4)
    plt.scatter(np.arange(len(Eigen_hist_max)), Eigen_hist_max, s = 3*markersize, label = 'MaxFreq')  
    plt.xlabel(r'Time')
    plt.ylabel(r'EVC Max Frequency')
    #plt.legend(fontsize = 'xx-large')
    plt.title('(d)', x = 0.1, y = 0.85, fontsize = 10)
    plt.subplots_adjust( wspace=0.40)
    plt.tight_layout(pad = 1)
        
    plt.savefig('collective_analysis/output_jpg/'  + matrix_type + '_Eigen_sec2av_{}_N_{}_d{}_tau{}_noise{}.jpg'.format(sec2av, N, dx, taux, noise_ampl), dpi = 300)

    plt.close()