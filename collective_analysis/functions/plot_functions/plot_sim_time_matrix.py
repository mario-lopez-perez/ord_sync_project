import copy
from matplotlib import pyplot as plt
import matplotlib


def plot_sim_time_matrix(sim_time_matrix, phase_space, metric = 'JS', hist = True, matrix_type = 'transcripts_PE', sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, nbins = 10): #The plot of the similarity matrix calculated by sim_time_matrix_calculate (sim_time_matrix) will be saved in output_jpg. The parameters are as defined in that function. 
    ax = plt.subplot(111)
    current_cmap = copy.copy(matplotlib.cm.get_cmap("coolwarm"))
    current_cmap.set_bad(color='white')
    im = plt.imshow(sim_time_matrix, interpolation = 'none')
    cbar = plt.colorbar(im)
    if metric == 'L2':
        cbar.set_label(r'$\zeta_{L^2}$', rotation = 'horizontal')  
    if metric == 'L1':
        cbar.set_label(r'$\zeta_{L^1}$', rotation = 'horizontal')  
    elif metric == 'JS':
        cbar.set_label(r'$\zeta_{JS}$', rotation = 'horizontal')          
    ax.set_xlabel('Time')
    ax.set_ylabel('Time')
    plt.tight_layout(pad = 0.05)

    if hist:
        plt.savefig('collective_analysis/output_jpg/' + matrix_type +'_sim_time_matrix_sec2av_{}_N_{}_d{}_tau{}_noise{}_metric{}_{}_hist{}.jpg'.format(sec2av, N, dx, taux, noise_ampl, metric, phase_space, nbins), dpi = 300)#, bbox_inches='tight')
      
    elif not hist:
        plt.savefig('collective_analysis/output_jpg/' + matrix_type +'_sim_time_matrix_sec2av_{}_N_{}_d{}_tau{}_noise{}_metric{}_{}.jpg'.format(sec2av, N, dx, taux, noise_ampl, metric, phase_space), dpi = 300)#, bbox_inches='tight')
     
    plt.close()    
