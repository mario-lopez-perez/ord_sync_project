
from sklearn.cluster import KMeans
import numpy as np
from matplotlib import pyplot as plt
import matplotlib
import copy

def plot_states_sequence_kmeans(phase_rep, phase_val, states, n_clusters, phase_space, matrix_type = 'transcripts_PE', markersize = 0.3, sec2av = 1, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, metric = 'L2', hist = True, nbins = 10): #This function plots the sequence of states as defined by K-Means algorithm, as encoded in states, an output of kmeans (see kmeans.py), with n_clusters clusters, which is an output of dendogram (see dendogram.py), as well as the ordered centroids of the clusters-states. It is saved in output_jpg. 
    n = phase_rep.shape[0]
    num_stocks = phase_rep.shape[1]
    fig, axs = plt.subplots(ncols=n_clusters, nrows=2, figsize = (3*n_clusters, 3*2) )
    for ax in axs[0, :]:
        ax.remove()

    gs = axs[1,1].get_gridspec()    

    colorlist = ['blue' if d not in [81, 180, 200]  else 'crimson' for d in range(len(states))] 
    markerlist = [80*markersize if d in [81, 180, 200] else 20*markersize for d in range(len(states))] #Ordinary points are blue circles; outlier points are big red dots.

    axbig = fig.add_subplot(gs[0, :])
    axbig.scatter(np.arange(len(states)), states, s = markerlist, c = colorlist)#, label = 'States-{}'.format(n_clusters))  
    axbig.set_xlabel(r'Time')
    axbig.set_ylabel(r'State')
    axbig.set_yticks(states)
    axbig.set_title('(a)', x = 0.01, y = 0.85, fontsize = 10)    
    
    mean_states = []
    for j in range(n_clusters):
        mean_state_j = np.nanmean(np.array([phase_rep[t, :] for t in range(phase_rep.shape[0]) if states[t] == j ]), axis = 0)
        if phase_space == 'all' and not hist:
            for k in range(mean_state_j.shape[0]):
                mean_state_j.reshape(n,)[k, k] = np.nan    
        mean_states.append(mean_state_j)
        
    for j in range(n_clusters):
        if phase_space == 'Deg':            
            axs[1, j].bar(phase_val, mean_states[j], width = phase_val[1] - phase_val[0], color = 'blue', alpha = 0.5)
            #axs[1, j].set_ylim((0,1))
        elif phase_space == 'EVC':            
            axs[1, j].bar(phase_val, mean_states[j], width = phase_val[1] - phase_val[0], color = 'red', alpha = 0.5)
            #axs[1, j].set_ylim((0,1))

        axs[1, j].set_xticks(phase_val)
        axs[1, j].set_xticklabels(["%.2f" % val for val in phase_val], rotation = 90)
        axs[1, j].tick_params(axis = 'x', which = 'major', labelsize = 5)
        axs[1, j].set_ylim((0, 1.05*np.nanmax(mean_states)))
        axs[1, j].set_yticks(np.arange(0, np.nanmax(mean_states), 5))                
        axs[1, j].set_xlabel(phase_space + '\n State {}'.format(j))
    axs[1, 0].set_title('(b)', x = 0.1, y = 0.85, fontsize = 10)            
    
    plt.tight_layout(pad = 1)  
    if hist:
        plt.savefig('collective_analysis/output_jpg/' + matrix_type +'_kmeans_States{}_sec2av_{}_N_{}_d{}_tau{}_noise{}_{}_hist{}.jpg'.format(n_clusters, sec2av, N, dx, taux, noise_ampl, phase_space, nbins), dpi = 300)
    elif not hist:
        plt.savefig('collective_analysis/output_jpg/' + matrix_type +'_kmeans_States{}_sec2av_{}_N_{}_d{}_tau{}_noise{}_{}.jpg'.format(n_clusters, sec2av, N, dx, taux, noise_ampl, phase_space), dpi = 300)
    plt.close()

