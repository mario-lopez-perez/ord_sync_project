import numpy as np
from matplotlib import pyplot as plt

def markov(matrix_type, prob_transition, prob_state, n_clusters, phase_space, algorithm, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, nbins = 10): #This function will plot the theoretical probabilities of the stationary distribution of the transitions Markov model of states dynamics and empirical frequencies of those transitions. 
    evals, evecs = np.linalg.eig(prob_transition.T)
    evec1 = np.real(evecs[:, np.isclose(evals, 1)].flatten())

    prob_stationary = evec1 / evec1.sum()

   # fig, axes = plt.subplots(1,2, figsize = (8, 4))
    #ax1 = axes[0]
    ax1 = plt.subplot(1,1,1)
    ax1.bar(np.arange(n_clusters), prob_state/prob_state.sum(), color = 'red', alpha = 0.7, label = 'Empirical Frequency', width = 0.4, align = 'edge')
    #ax1.set_title('Empirical Frequency of States')
    #ax1.set_xlabel('State')
    #ax1.set_ylabel('Frequency')    
    #ax1.set_ylim((0,1))

    #ax2 = axes[1]
    ax1.bar(np.arange(n_clusters), prob_stationary, color = 'blue', alpha = 0.7, label = 'Theoretical Probability', width = -0.4, align = 'edge')
#    ax1.set_title(r'Empirical Frequency of States and Stationary Distribution of $P$')
    ax1.set_xlabel('State')
    ax1.set_xticks(np.arange(n_clusters))    
    ax1.set_ylabel('Frequency/Probability')  
    ax1.set_ylim((0,1))      
    ax1.legend(fontsize = 15)
    if phase_space == 'Deg':
        ax1.set_title('(b)', x = 0.1, y = 0.85, fontsize = 12)               
    elif phase_space == 'EVC':
        ax1.set_title('(d)', x = 0.1, y = 0.85, fontsize = 12)               
        
    plt.tight_layout(pad = 1)
    
    plt.savefig('collective_analysis/output_jpg/' + matrix_type + '_' + algorithm + '_Stationary_dist_States{}_sec2av_{}_N_{}_d{}_tau{}_noise{}_{}_hist{}.jpg'.format(n_clusters, sec2av, N, dx, taux, noise_ampl, phase_space, nbins), dpi = 300)   
   
    plt.close()

