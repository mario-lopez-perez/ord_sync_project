import numpy as np
from functions.data_functions.dynamical_histogram import *
  
def phase_representation(array, phase_space, hist = True, nbins = 10): #This function returns the phase representation to be used in the collective analysis. In the paper we use EVC and Degree, but you can use other phase representation, in order to later measure distances between whole adjacency matrix or their histograms instead of using degree or eigenvector centrality vectors. 
    n = array.shape[0]
    array.reshape(n, -1)   

    phase_val = [] #If you prefer to measure distances between whole matrices or vectors instead of using their histograms, set hist = False. In that case ignore phase_val, it will not be used. 

    if hist: #This 'if' uses the dynamical_histogram function above defined. The option of considering distances between whole matrices or vectors could be prefered in some cases, and then hist must be set equal to 'False' (in the paper it is not). 
        phase_rep, phase_val = dynamical_histogram(array[: , :], nbins = nbins)
        phase_rep[np.isnan(phase_rep)] = 0 

    return phase_rep, phase_val    