from scipy.spatial.distance import jensenshannon as JSD_scipy
from scipy.spatial.distance import pdist
from scipy.cluster import hierarchy
from matplotlib import pyplot as plt
import numpy as np

def JSD(p,q): #Jensen-Shannon Distance will be used with logarithms base 2.
    return JSD_scipy(p, q, base = 2)
    
def dendogram(phase_rep, phase_val, matrix_type, phase_space, metric = 'JS', t = 0.14, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, hist = True, nbins = 10): #This funtions will calculate the dendogram. It returns states_noise, the sequence of integers indicating the cluster to which it belongs. Clusters are ordered and labeled after the distance from centroids to uniform distribution: the nearest is state 0, the next is state 1, and so on. Noise point are labeled as -1. It also returns states, the same sequence but with noise removed; n_clusters, the number of clusters determined by the cut-off threshold t; and coph_corr, the cophenetic index of the dendogram. phase_rep is the phase representation, in the paper the dynamical histogram of Degree or EVC, phase_val is the sequence of middle points of bins; t is the cut-off threeshold to stop merging clusters. The other parameters are as defined in main_collective.py. 
    n = phase_rep.shape[0]   
    if metric == 'JS':
        Y = pdist(phase_rep, metric = JSD)
    elif metric == 'L2':
        Y = pdist(phase_rep, metric = 'euclidean')
#    elif metric == 'cos':
#        Y = pdist(phase_rep, metric = 'cosine')
    Z = hierarchy.linkage(Y, 'single')
    coph_coeff, _ = hierarchy.cophenet(Z = Z, Y = Y) 
    

    #The next lines are to order the clusters by the distance of their centroids to uniform distribution.
    labels_ = hierarchy.fcluster(Z, t = t, criterion = 'distance') 
    labels_unique = np.unique(labels_)
    labels_noise = np.zeros(len(labels_))
    i = 0
    for j in labels_unique:
        ind_j = np.argwhere(labels_ == j).flatten()
        if len(ind_j) < 4:
            labels_noise[ind_j] = -1            
        else:    
            labels_noise[ind_j] = i
            i += 1
    labels_ = labels_noise[labels_noise != -1]        
    labels_unique = np.unique(labels_)        
    n_clusters = len(labels_unique)            
    centers = np.zeros((n_clusters, phase_rep.shape[1]))    
    for i, lab in zip(np.arange(n_clusters), labels_unique):  
        ind_lab = np.argwhere(labels_noise == lab).flatten()
        centers[i, :]= np.nanmean(phase_rep[ind_lab, :], axis = 0) 
    unif_dist = np.ones(phase_rep.shape[1])/nbins 
    idx = np.argsort(np.array([JSD(centers[i, :], unif_dist) for i in range(n_clusters)]))
#    idx = np.argsort(np.array([np.linalg.norm(centers[i, :]) for i in range(n_clusters)]))
#    idx = np.argsort(np.nanmax(centers, axis = 1))    
    lut = np.zeros_like(idx)#lookup table to sort the labels by magnitude
    lut[idx] = np.arange(n_clusters)
    states = lut[labels_.astype(int)].astype(int)
    states_noise = -np.ones(len(labels_noise))
    states_noise[np.argwhere(labels_noise != -1).flatten()] = states
    
    return Z, states_noise, states, n_clusters, coph_coeff