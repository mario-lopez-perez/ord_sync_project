import numpy as np
import pandas as pd
from numba import njit, prange
from itertools import product
from functions.data_functions.transcript_entropy import *
import os 

@njit(parallel = True)
def avarray(meanprice, tradefreq, sec2av = 5): #This function averages the original dataframe in blocks of sec2av seconds. mean_price is the dataset of logarithms of prices, while tradefreq is the dataset of number of transactions per second of a given asset. 
    A = np.zeros((len(meanprice[:, 0]), len(range(0, len(meanprice[0, :]) - sec2av, sec2av))))
    for i in prange( len(meanprice[:, 0])):
        for j in range(len(range(0, len(meanprice[0, :]) - sec2av, sec2av))):
            if np.nansum(tradefreq[i, j*sec2av: (j + 1)*sec2av]) != 0:
                A[i, j] = np.nansum( meanprice[i, j*sec2av: (j + 1)*sec2av]*tradefreq[i, j*sec2av: (j+1)*sec2av] )/ np.nansum(tradefreq[i, j*sec2av: (j + 1)*sec2av]) 
            else:
                A[i, j] = np.nan
    return A 

def data_format(listfile_meanpr, n, len_series, sec2av = 5, N = 1, num_stocks = 24): #This function returns an array, called 'Data', with the data of all stocks in a suitable format in order to build the dynamical network. The axis 0 corresponds to time: it indicates a sliding window of N trading days; those windows are daily windows in the paper. The axis 1 contains the terms of such window, that is, the differences of mean logarithms of prices. The axis 2 indicates the stock.  In this way, Data[t, :, k] is the series of differences of mean logarithms of prices of the stock k during the t-th subseries of N trading days. listfile_meanpr is the list of paths of csv files containing the original data: logarithms of mean prices per second of each stock; n is the number of subseries of N trading days we will use to construct the networks conforming our dynamical network; len_series is the length of such a subseries; sec2av, N and num_stocks are as defined in main_collective.
    Data = np.zeros((n, len_series , num_stocks)) 
    for k in range(num_stocks):
        dfpath = listfile_meanpr[k]
        dfname = os.path.splitext(os.path.basename(dfpath))[0]
        meanprice = pd.read_csv(dfpath) 
        tradefreq = pd.read_csv('data/Trading_frequency/{}.csv'.format(dfname))
        avdata = avarray(np.array(meanprice.iloc[:, 1: ]), np.array(tradefreq.iloc[:, 1: ]), sec2av)
        avincrements = avdata[:, 1: ] - avdata[:, : -1]
        for t in range(n):
            Data[t, :, k] = avincrements[t: t + N, :].flatten()
            #num_nan = np.count_nonzero(np.isnan(Data[t, :, k]))
            #if num_nan > 0.75*Data.shape[1]:
            #    Data[t, :, k] = Data[t - 1, :, k]
    return Data


def dynamical_network_csv(Data, matrix_type = 'transcripts_PE', sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl =  0.0000001, random_seed = 0): #This function will take the data previosly formatted in main_collective.py and build and save the dynamical network in a csv file in output_csv. Data is the formatted data of all stocks, matrix_type could be 'transcripts_PE' or 'corr'. The first option is for defining the weights of the network bu using transcript entropy, as defined in the paper. The second option is to do the same by using (linear) correlation coefficients, and could be used in order to compare results; n_stocks is the number of stocks; sec2av is the number of seconds used to average logarithms of mean prices with avarray; N is the length in trading days of the subseries on which we measure our coefficient (transcript entropy or linear correlation); dx is the dimension of ordinal patterns, in the paper is set equal to 5; taux is a parameter for ordpy, all through our work set equal to 1 (see documentation), noise_ampl is the amplitude of uniform noise added to our original series in order to randomly break equalities as explained in the paper. IMPORTANT: NOTE THAT, IF YOU WANT TO MAKE RESULTS REPRODUCIBLE, YOU MUST ENSURE CONSISTENCY IN RANDOM SEED random_seed USED WHEN MEASURING TRANSCRIPT ENTROPY (see below). IF YOU DON'T WANT TO ADD ANY NOISE, YOU ARE FREE TO SET random_seed equal to None, but that will led to spurious conclusions by detecting patterns which are not there, in particular, increasing patterns.
    n = Data.shape[0] #Time: number of corresponfing sliding window of N trading days .
    num_stocks = Data.shape[2] #number of stocks

    corr_matrix = np.zeros((n, num_stocks, num_stocks)) #This will store the weights of the dynamical network. Axis 0 represents time: indicates the sliding window in question, so corr_matrix[t, :, :] contains the n_stocks x n_stocks dimensional matrix indicating the edge weights of the t-th network, in the paper the t-th trading day.
    if matrix_type == 'corr':    
        for t in range(n):
            corr_matrix[t, :, :] = np.array(pd.DataFrame(Data[t, :, :]).corr())
            
    
    elif matrix_type == 'transcripts_PE':
        corr_matrix = np.zeros((n, num_stocks, num_stocks)) 
        if random_seed != None: #If a random seed is specified as an int, then a uniform noise of amplitude noise_ampl and random seed random_seed will be added to the data, to randomly break inequailities as explained in the paper.
            np.random.seed(random_seed)
            noise = noise_ampl*np.random.rand(Data.shape[0], Data.shape[1], Data.shape[2])
            Data = Data + noise
        for t in range(n): 
            print('day {}'.format(t))
            for stock_1 in range(num_stocks):
                for stock_2 in range(stock_1 + 1, num_stocks):
                    indnonan = ~np.isnan(Data[t, :, stock_1])*~np.isnan(Data[t, :, stock_2]) #The set of indices where both series are not nan.
                    corr_matrix[t, stock_1, stock_2 ] = 1 - transcript_entropy(Data[t, indnonan, stock_1] , Data[t, indnonan, stock_2], dx = dx, taux = taux)  
                    corr_matrix[t, stock_2, stock_1 ] = corr_matrix[t, stock_1, stock_2 ]
                corr_matrix[t, stock_1, stock_1] = 1

    
            
    days_index, stock1_index, stock2_index  = corr_matrix.shape
    days_index_, stock1_index_, stock2_index_ = zip(*product(range(days_index), range(stock1_index), range(stock2_index)))
    pd.DataFrame(corr_matrix.flatten()).assign(days_index=days_index_, stock1_index=stock1_index_, stock2_index=stock2_index_).to_csv('collective_analysis/output_csv/dynamical_' + matrix_type +'_matrix_sec2av_{}_N_{}_d{}_tau{}_noise{}.csv'.format(sec2av, N, dx, taux, noise_ampl)) #These three lines save the dynamical matrix in a csv file in output_csv folder; this is done to avoid repeting heavy calculations. We flatten our 3D array in order to better acces it later.
        
    