import numpy as np
from ordpy import ordinal_sequence
from math import factorial as fac 


def ind_pis(pis_sequence, pis_unique):#This function is defined to save time. When applying the function transcript_entropy (see below) we will need to match certain couples of arrays (ordinal patterns), and this is more effectively accomplished by searching for their index in an absolute list, for which we need to match arrays just once. pis_sequence is an array of ordinal patterns and pis_unique is the absolute list of patterns from which we extract the index.
    ind = np.zeros(len(pis_sequence))
    for t in range(len(pis_sequence)):
        for i in range(len(pis_unique)):
            if np.all(pis_unique[i, :] == pis_sequence[t, :]):
                ind[t] = i
                break
    return ind

def transcript_entropy(X, Y, dx = 5, taux = 1): #This function measures transcript entropy as defined in the paper. Please note that, in order to make results reproducible, you should ensure consistency in the random seed generating equality-breaking noises. X and Y are the time series whose transcript synchronization is measured. The other arguments are as defined in main_collective.py.
    ord_sequence_1 = ordinal_sequence(X , dx = dx, taux = taux)
    ord_sequence_2 = ordinal_sequence(Y , dx = dx, taux = taux)
    transcript_series = np.array([ord_sequence_2[j][np.argsort(ord_sequence_1[j])] for j in range(len(ord_sequence_1))])
    transcripts = np.unique(transcript_series, axis = 0)
    transcript_index = ind_pis(transcript_series, transcripts)
    prob_transcript = np.zeros(len(transcripts))
    for j in range(len(transcripts)):
        prob_transcript[j] = np.count_nonzero([j == transcript_index[t] for t in range(len(transcript_series))]) / len(transcript_series) 
    return -np.nansum(prob_transcript*np.log(prob_transcript))/np.log(fac(dx))
