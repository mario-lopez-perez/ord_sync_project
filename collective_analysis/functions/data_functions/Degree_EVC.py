import numpy as np
import networkx as nx



def Degree_EVC(corr_matrix): #This function calculates Degree and Eigenvector Centrality (EVC) for the dynamical network given by the dynamical adjacency matrix corr_matrix. corr_matrix[t, :, :] is the weighted adjacency matrix of the t-th network.
    n = corr_matrix.shape[0] 
    num_stocks = corr_matrix.shape[1]

    corr_network = [nx.Graph() for t in range(n)] #We use networkx to easily calculate degree and eigenvector centrality. Here we define our dynamical network as a list of n networks
    for i in range(num_stocks):
        for t in range(n):
            corr_network[t].add_node(i)    
            
    Degree = np.zeros((n, num_stocks))
    Eigen_centrality = np.zeros((n, num_stocks))


    for t in range(n):
        for i1 in range(num_stocks):
            for i2 in range(i1+1, num_stocks):
                w = np.abs(corr_matrix[t, i1, i2])
                corr_network[t].add_edge(i1, i2, weight = w) 
        Degree[t, :] = [deg for item, deg in corr_network[t].degree(weight = 'weight')]
        try:
            Eigen_centrality[t, :] = list(nx.eigenvector_centrality_numpy(corr_network[t], max_iter = 600, tol=1e-05, weight = 'weight').values() )
        except:
            Eigen_centrality[t, :] = [np.nan for i in range(num_stocks)]


    return Degree, Eigen_centrality