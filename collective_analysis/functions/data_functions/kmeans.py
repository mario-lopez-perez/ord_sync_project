from sklearn.cluster import KMeans
import numpy as np


def kmeans_states(phase_rep, phase_val, n_clusters, phase_space, matrix_type = 'transcripts_PE', sec2av = 1, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, metric = 'L2', hist = True, nbins = 10): #This funtions will carry out the clustering process as definded by K-Means algorithm. It returns states, the sequence of integers indicating the cluster to which it belongs. Clusters are ordered and labeled after the distance from centroids to uniform distribution: the nearest is state 0, the next is state 1, and so on; phase_rep is the phase representation, in the paper the dynamical histogram of Degree or EVC, phase_val is the sequence of middle points of bins. The other parameters are as defined in main_collective.py. 
    n = phase_rep.shape[0]
    num_stocks = phase_rep.shape[1]
    kmeans = KMeans(n_clusters = n_clusters, random_state = 0).fit(phase_rep)    
    idx = np.argsort(np.linalg.norm(kmeans.cluster_centers_, axis=1))
#    idx = np.argsort(np.nanmax(kmeans.cluster_centers_, axis = 1))        
    lut = np.zeros_like(idx)#lookup table to sort the labels by magnitude
    lut[idx] = np.arange(n_clusters)
    states = lut[kmeans.labels_].astype(int)
    return states