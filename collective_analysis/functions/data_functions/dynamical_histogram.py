import numpy as np


def dynamical_histogram(array, nbins = 10, quantile_min = 0.00, quantile_max = 1):
    min_array = np.nanquantile(array, quantile_min)
    max_array = np.nanquantile(array, quantile_max)
    array_hist = np.zeros((array.shape[0], nbins))
    for t in range(array.shape[0]):
        array_hist[t, :], bins = np.histogram(array[t, :], bins = nbins, range = (min_array, max_array), density = True)
    bins_middle_points = np.array([bins[0] + (2*i+1)*(bins[i+1]-bins[i])/2 for i in range(len(bins)-1)])
    return array_hist, bins_middle_points
