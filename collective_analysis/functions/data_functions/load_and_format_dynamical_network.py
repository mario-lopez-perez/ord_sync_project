import numpy as np
import pandas as pd


def load_dynamical_network(Data_path, num_stocks): #This function will load the previously calculated and saved dynamical adjacency matrix (Data). This is done in order to avoid repetition of heavy calculations.
    Data = pd.read_csv(Data_path) 
    corr_matrix = np.array(Data.iloc[:, 1]).reshape(-1, num_stocks, num_stocks)
    return corr_matrix