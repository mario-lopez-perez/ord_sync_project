   
from matplotlib import pyplot as plt
import matplotlib
import numpy as np
from scipy.spatial.distance import jensenshannon as JSD_scipy

def JSD(p,q): #Jensen-Shannon Distance will be used with logarithms base 2.
    return JSD_scipy(p, q, base = 2)

def sim_time_matrix_calculate(phase_rep, phase_space, metric = 'JS', hist = True, matrix_type = 'transcripts_PE', sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, nbins = 10): # This function will calculate the simmilarity matrix of our phase representation. It will return a matrix in which term ij corresponds to the distance between our i-th and j-th networks, according to the specific phase representation and metric indicated. phase_rep is the dynamical vector, which can be histograms of Degree or EVC (see paper), but also other representations such as the whole Degree vectors or adjacency matrices; phase_rep tell the function which representation is being used; metric indicates the distance, which can be L1 or L2 norm, or, as in the paper, Jensen-Shannon distance 'JS'. The other parameters are as definid in main_collective.py.
    sim_time_matrix = np.zeros((phase_rep.shape[0], phase_rep.shape[0]))
    if metric == 'L1':     
        for t1 in range(phase_rep.shape[0]):
            for t2 in range(phase_rep.shape[0]):
                sim_time_matrix[t1, t2] = np.nansum(np.abs(phase_rep[t1, :] - phase_rep[t2, :]))
    if metric == 'L2':     
        for t1 in range(phase_rep.shape[0]):
            for t2 in range(phase_rep.shape[0]):
                sim_time_matrix[t1, t2] = np.linalg.norm(phase_rep[t1, :] - phase_rep[t2, :])
    elif metric == 'JS':# and hist:
        for t1 in range(phase_rep.shape[0]):
            for t2 in range(phase_rep.shape[0]):
                sim_time_matrix[t1, t2] = JSD(phase_rep[t1, :], phase_rep[t2, :])
    return sim_time_matrix      


