#This script is the one you have to compile to reproduce csv and jpg files from the article "Ordinal Synchronization and Typical States in High-Frequency Digital Markets", submitted to Physica A by Mario López and Ricardo Mansilla. It will save csv files in output_csv and jpg files in output_jpg. It corresponds to the collective analysis of stocks.
import numpy as np
import pandas as pd
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import os
import glob
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import copy
from numba import njit, prange
from scipy.stats import entropy
import time
from matplotlib import cm
import matplotlib 
from sklearn.cluster import KMeans
from ordpy import ordinal_sequence, ordinal_distribution
from math import factorial as fac, tau 
from itertools import product
from functions.data_functions.dynamical_network_csv import *
from functions.data_functions.load_and_format_dynamical_network import *
from functions.data_functions.Degree_EVC import *
from functions.data_functions.dynamical_histogram import *
from functions.plot_functions.plot_dynamical_histogram import *
from functions.data_functions.phase_representation import *
from functions.data_functions.sim_time_matrix import *
from functions.plot_functions.plot_sim_time_matrix import *
from functions.plot_functions.plot_Deg_EVC import *
from functions.data_functions.dendogram import *
from functions.plot_functions.plot_dendogram import *
from functions.plot_functions.plot_states_sequence_dendogram import *
from functions.data_functions.kmeans import *
from functions.plot_functions.plot_states_sequence_kmeans import *
from functions.plot_functions.plot_states_color_hist import *
from functions.plot_functions.plot_transition_prob import *
from functions.plot_functions.plot_markov import *

Labels = ['ABT', 'BMY', 'PFE', 'BAC', 'C', 'USB', 'WFC', 'CSCO', 'HPQ', 'INTC',  'FB', 'MSFT', 'ORCL', 'TWTR', 'FOXA', 'GE', 'F', 'GM', 'KO', 'MDLZ', 'MO', 'MS', 'T', 'VZ'] #The codes of our stocks.

listfile_meanpr = ['data/Mean_price/{}.csv'.format(dfname) for dfname in Labels] #We group the assets by economic sector; this has no effect, but to present figures as those in the paper.
num_stocks = len(listfile_meanpr) #Number of stocks (24)
markersize = 0.3 #This will be a standard reference size to plot figures.
sec2av = 5 #This is the number of seconds we used in the paper to average logarithms of mean prices. In the paper is called \tau and equals 5.
N = 1 #This is the length of the time subseries in trading days, the information-theoretic measures are applied to them. In the paper equals 1.
noise_ampl = 0.0000001 #noise_ampl is the amplitude of the uniform noise we add to our subseries in order to randomly break equalities, as explained in the paper (see dynamical_network_csv.py for details of how this works).
random_seed = 0 #IMPORTANT: NOTE THAT, IF YOU WANT TO MAKE RESULTS REPRODUCIBLE, YOU MUST ENSURE CONSISTENCY IN RANDOM SEED random_seed USED WHEN MEASURING TRANSCRIPT ENTROPY (see dynamical_network_csv.py for details). IF YOU DON'T WANT TO ADD ANY NOISE, YOU ARE FREE TO SET random_seed equal to None, but that will led to spurious conclusions by detecting patterns which are not there, in particular, increasing patterns.
taux = 1 #taux is the time lag parameter for ordinal patterns, in the paper it is called l.
dx = 5 #The dimension of ordinal patterns, called m in the paper. In the paper equals 5.       
hist = True #This boolean is set equal to True in the paper, and indicates the phase space is histogramized with nbins (see below). DON'T CHANGE THIS UNLESS YOU APPROPIATELY MODIFY OUT FUNCTIONS TO DEAL WITH NON-HISTOGRAMIZED PHASE SPACES. Although those modifications would be straight forward, it is very important to asure consistency throughout the code. We will include them in a later version of this repository.
nbins = 10 #This is the number of bins we use to calculate dynamical histograms as phase representations. In the paper it is set equal to 10.
metric = 'JS' #This is the metric used to calculate the similarity matrix (see sim_time_matrix.py) and dendogram (see dendogram.py). 'JS' stands for Jensen-Shannon distance, and it is appropiate just for histogramized phase representations (hist = True); other options are 'L1' and 'L2', which stand for the respective norms. K-Means algorithm is always calculated with 'L2'.



matrix_type = 'transcripts_PE'  #This string specifies the type of correlation used to construct the dynamical network. 'transcript_PE' is to measure transcript synchronization networks, while 'corr' does the same for (linear) correlation instead.
#matrix_type = 'corr' #Uncomment this line if you want to produce figures and datasets for (linear) correlation matrixes.


#####This block is just to easily obtain from the first csv file of the list the number n of N-days subseries and the length of N-days subseries we will use to construct our dynamical networks.
dfpath = listfile_meanpr[0]
dfname = os.path.splitext(os.path.basename(dfpath))[0]
meanprice = pd.read_csv(dfpath) 
n = meanprice.shape[0] - N
len_series = ((meanprice.shape[1] - 1) // sec2av - 1)*N  #len_series is the length of N-days subseries: meanprice.shape[1] is number of seconds in a trading day; we substract 1 because we don't want to count the first column, which is of row labels; we divide by sec2av because we will average in blocks of sec2av seconds; we substract 1 because we will take differences of sec2av-averaged blocks; finally, we multiply by N because we will consider subseries of N trading days.    
##### 


Data = data_format(listfile_meanpr = listfile_meanpr, n = n, len_series = len_series, N = N, num_stocks = num_stocks) #This will collect and format our data in order to later access it in a practical way. It contains the differences of averages of logarithms of mean prices of all stocks. This line should be commented after compiled once, in order to avoid repetition of heavy calculations


dynamical_network_csv(Data, matrix_type = matrix_type, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl =  noise_ampl, random_seed = random_seed) #This will calculate the edge weights of our dynamical network through the trading year and save this data in a csv file in output_csv in order to not repeat heavy calculations. After run once, this line should be commented, and the data accessed through that csv file, as in the next line. 


Data_path = 'collective_analysis/output_csv/dynamical_' + matrix_type +'_matrix_sec2av_{}_N_{}_d{}_tau{}_noise{}.csv'.format(sec2av, N, dx, taux, noise_ampl) #This is the path to the csv file saved by dynamical_network_csv one and two lines above. This line is useful in order not to repeat those heavy calculations. If you are compiling this file to reproduce data and figures from the article for the first time, those lines must be uncommented; otherwise they should be commented.

corr_matrix = load_dynamical_network(Data_path, num_stocks) #Here we load and format the data given by Data_path above.

Degree, Eigen_centrality = Degree_EVC(corr_matrix) #This line gives us, from the dynamical network, its Degree and EVC dynamical vectors, so Degree and Eigen_centrality are numpy arrays of shape (n, num_stocks), and Degree[t, i] is the degree of listmeanpr i-th stock during the t-th subseries or sliding window, and similarly for Eigen_centrality.

Degree = Degree[1: , :] 
Eigen_centrality = Eigen_centrality[1: , :] #This line and the previous one are to exclude the first trading day of our analysis, as explained in the paper. This can be modified to better understand the nature of that outlier day, but at the cost of altering significatevely the grasp of the more general dynamics, particulaely for Degree phase space.

Degree_hist, Degree_vals = dynamical_histogram(Degree, nbins = 10)
Eigen_hist, Eigen_vals = dynamical_histogram(Eigen_centrality, nbins = nbins) #This line and the previous one are to calculate the histograms of Degree and Eigenvector Centrality, in order to analyze this dynamical histogram as the phase representation of our dynamical network. Degree_hist is a (n, nbins) array, with Degree_hist[t, b] the frequency of degree the b-th bin during t-th sliding window, and Eigen_vals is a one-dimensional array of length nbins containing the middle point of such bins, and similarly for Eigen_hist and Eigen_vals.

##HERE BEGINS THE PLOTTING OF THE FIGURES OF SECTION 5 "Collective Analysis of Stocks through Transcript Synchronicity Dynamical Networks"
plot_3Dhist_surface(array_hist = Eigen_hist, array_values = Eigen_vals, phase_space = 'EVC', matrix_type = matrix_type, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, nbins = nbins)#Plots the evolution of histograms of EVC.

plot_3Dhist_surface(array_hist = Degree_hist, array_values = Degree_vals, phase_space = 'Deg', matrix_type = matrix_type, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, nbins = nbins)#Plots the evolution of histograms of Degree.


plot_measures_deg_EVC(Degree = Degree, Degree_hist = Degree_hist, Eigen_centrality = Eigen_centrality, Eigen_hist = Eigen_hist, matrix_type = matrix_type, markersize = markersize, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl) #Plots the evolution of mean, standard deviation, minimum value and maximum frequency of Degree and EVC.

plot_deg_EVC(Degree = Degree, Eigen_centrality = Eigen_centrality, Labels = Labels, matrix_type = matrix_type, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl) #Plots the evolution of Degree and EVC vectors as functions of stocks through the year.

##HERE BEGINS THE PLOTTING OF THE FIGURES OF SECTION 6 "Clustering Analysis"
for phase_space in ['Deg', 'EVC']: #This for is to calculate and plot figures for both phase spaces. 'EVC' stands for eigenvector centrality and 'Deg' for Degree.
    if phase_space == 'EVC':
        array = Eigen_centrality
        t = 0.14
    elif phase_space == 'Deg':
        array = Degree
        t = 0.13 #t is the threshold for the dendogram, it indicates when to stop the merging process, and thus determines the number of clusters. It was visually chosen in the paper, and is a very sensitive parameter, to be very carefully calibrated.


    phase_rep, phase_val = phase_representation(array, phase_space = 'phase_space', hist = hist, nbins = nbins)


    
    sim_time_matrix = sim_time_matrix_calculate(phase_rep = phase_rep, phase_space = phase_space, metric = metric, hist = hist, matrix_type = matrix_type, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, nbins = nbins) #his function will calculate the simmilarity matrix of our phase representation. It will return a matrix in which term ij corresponds to the distance between our i-th and j-th networks, according to the specific phase representation and metric indicated. phase_rep is the dynamical vector, which can be histograms of Degree or EVC (see paper), but also other representations such as the whole Degree vectors or adjacency matrices; phase_rep tell the function which representation is being used; metric indicates the distance, which can be L1 or L2 norm, or, as in the paper, Jensen-Shannon distance 'JS'. The other parameters are as defined above.

    plot_sim_time_matrix(sim_time_matrix = sim_time_matrix, phase_space = phase_space, metric = metric, hist = hist, matrix_type = matrix_type, sec2av = sec2av, N = 1, dx = dx, taux = taux, noise_ampl = noise_ampl, nbins = nbins) #This function plots the similarity matrix returned by sim_time_matrix_calculate.


    Z, states_noise_dendogram, states_dendogram, n_clusters, coph_coeff = dendogram(phase_rep = phase_rep, phase_val = phase_val, matrix_type = matrix_type, phase_space = phase_space, metric = metric, t = t, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, hist = hist, nbins = nbins) #This funtions will calculate the dendogram Z, the labels states_noise and states of the subseries of N days according to the ordered cluster they belong to, the number n_clusters of clusters given by the cut-off threshold t and the cophenetic coefficient coph_coeff (see dendogram.py for further information).

    plot_dendogram(Z = Z, t = t, matrix_type = matrix_type, phase_space = phase_space, coph_coeff = coph_coeff, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, metric = metric, nbins = nbins)  #This function plots the dendogram Z calculated in dendogram.py, indicating threshold t with an horizontal line and displaying the cophenetic coefficient coph_corr.

    plot_states_sequence_dendogram(phase_rep = phase_rep, phase_val = phase_val, states_noise = states_noise_dendogram, states = states_dendogram, n_clusters = n_clusters, phase_space = phase_space, matrix_type = matrix_type, markersize = markersize, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, metric = metric, nbins = nbins) #This function plots the sequence of states and noise as defined by states_noise, output of dendogram (see dendogram.py), as well as the ordered centroids of the clusters-states. It is saved in output_jpg.

    states_kmeans = kmeans_states(phase_rep = phase_rep, phase_val = phase_val, n_clusters = n_clusters, phase_space = phase_space, matrix_type = matrix_type, sec2av = sec2av , N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, metric = 'L2', hist = hist, nbins = nbins) #This funtions will carry out the clustering process as definded by K-Means algorithm. It returns states, the sequence of integers indicating the cluster to which it belongs. Clusters are ordered and labeled after the distance from centroids to uniform distribution: the nearest is state 0, the next is state 1, and so on. The metric is always 'L2' and changing it will make no difference. The number of clusters is n_clusters, an output of dendogram (see dendogram.py).
    
    plot_states_sequence_kmeans(phase_rep = phase_rep, phase_val = phase_val, states = states_kmeans, n_clusters = n_clusters, phase_space = phase_space, matrix_type = matrix_type, markersize = markersize, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, metric = metric, hist = hist, nbins = nbins) #This function plots the sequence of states as defined by K-Means algorithm, as encoded in states, an output of kmeans (see kmeans.py), with n_clusters clusters, which is an output of dendogram (see dendogram.py), as well as the ordered centroids of the clusters-states. It is saved in output_jpg.

    for algorithm in ['dendogram', 'kmeans']: #We use this for loop to reuse funtions which can be applied to both states dynamics, those given by dendogram and those given by K-Means.
        if algorithm == 'kmeans':
            states = states_kmeans
        elif algorithm == 'dendogram':
            states = states_dendogram
        plot_3Dhist_bars(phase_rep = phase_rep, phase_val = phase_val, states_noise = states_noise_dendogram, states = states, n_clusters = n_clusters, phase_space = phase_space, algorithm = algorithm, matrix_type = matrix_type, markersize = markersize, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, metric = metric, nbins = nbins) #This function plots the sequence of states and noise as defined by states_noise, output of dendogram (see dendogram.py), in the form of histograms colored after the state they belong to. It is saved in output_jpg. the parameter algorithm can be 'kmeans' or 'dendogram', and indicates which algorithm was used to obtain the states dynamics.

        #HERE BEGINS THE PLOTTING OF THE FIGURES OF SECTION 7, "A Markov Model for State Transitions".

        prob_transition, prob_state = transition_prob(states = states, n_clusters = n_clusters, phase_space = phase_space, matrix_type = matrix_type, algorithm = algorithm, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, nbins = nbins) #This function plots the transition probabilities defined by the states dynamics, encoded in states, where states is an output of dendogram (see dendogram.py) or kmeans (see kmeans.py), and n_clusters is the number of clusters given by dendogram. It also returns the transition probabilities, used below to plot Markov model graphs.   
        
        markov(matrix_type = matrix_type, prob_transition = prob_transition, prob_state = prob_state, n_clusters = n_clusters, phase_space = phase_space, algorithm = algorithm, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, nbins = nbins) #This function will plot the theoretical probabilities of the stationary distribution of the transitions Markov model of states dynamics and empirical frequencies of those transitions.