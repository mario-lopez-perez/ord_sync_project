## Ordinal Synchronization and Typical States in High-Frequency Digital Markets

This repository contains data and code to reproduce the results of the article *Ordinal Synchronization and Typical States in High-Frequency Digital Markets*, published in *Physica A: Statistical Mechanics and its Applications, vol. 598 (2022)*, DOI: https://doi.org/10.1016/j.physa.2022.127331, by Mario López and Ricardo Mansilla. It is divided in three folders:

The folder "data" contains two folders, "Main_price", which contains the datasets of logarithms of mean prices per second of each stock, and "Trading_Frequency", containing the number of transactions per second. Both of them are formatted as follows: each row contains the data from one trading day, and each column corresponds to a second of the trading day.

The remaining folders are kept separate in case somebody wants to reproduce just a part of the work.
 
The folder 'individual_analysis' contains the code to reproduce the individual analysis of stocks carried out in section 4 of the paper. The file you have to compile to do this is main_individual.py. See 'README.txt' in that folder for more information.

The folder 'collective_analysis' contains the code to reproduce the collective analysis of stocks carried out in sections 5-7 of the paper. The file you have to compile to do this is main_collective.py. See 'README.txt' in that folder for more information.
