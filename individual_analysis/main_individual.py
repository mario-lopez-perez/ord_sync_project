#This script is the one you need to compile to reproduce csv and jpg files from the article "Ordinal Synchronization and Typical States in High-Frequency Digital Markets", submitted to Physica A by Mario López and Ricardo Mansilla. It will save csv files in output_csv and jpg files in output_jpg
import os
import glob
from Functions.individual_analysis_csv import *
from Functions.individual_analysis_jpg import *
#listfile_meanpr = glob.glob('data/Mean_price/*.csv') #The list of files containing datasets of logarithms of mean prices of all assets. Uncomment if you want to produce all the figures, not shown in the paper.
listfile_meanpr = ['data/Mean_price/' + dfname + '.csv' for dfname in ['T']]#, 'TWTR'] ] #This line is to reproduce the two figures from the paper.
markersize = 0.3 #This will be a standard reference size to plot figures.
sec2av = 5 #This is the number of seconds we used in the paper to average logarithms of mean prices. In the paper is called \tau.
N = 1 #This is the length of the time subseries in trading days, the information-theoretic measures are applied to them.
noise_ampl = 0.0000001 #noise_ampl is the amplitude of the uniform noise we add to our subseries in order to randomly break equalities.
taux = 1 #tau is a parameter of several functions from ordpy, we set it equal to one (see ordpy documentation)
dx = 5 #The dimension of ordinal patterns, called m in the paper.        

for dfpath in listfile_meanpr: 
    dfname = os.path.splitext(os.path.basename(dfpath))[0] #asset name
    meanprice = pd.read_csv(dfpath) #The original dataset, containing logarithms of mean prices per second
    tradefreq = pd.read_csv('data/Trading_frequency/{}.csv'.format(dfname))#The original dataset of number of transactions per second  
       
    individual_analysis_csv(meanprice = meanprice, tradefreq = tradefreq, dfname = dfname, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl) #This will save the calculated information-theoretic measures in ourput_csv, this is done in order to not repeat the calculations each time we need to reproduce or try new figures. This line should be commented once you have run the script for the first time.

            
    Data = pd.read_csv('individual_analysis/output_csv/' + dfname + '_ord_measures_sec2av_{}_N{}_d{}_tau{}_noise{}.csv'.format(sec2av, N, dx, taux, noise_ampl))   #This line is used to load the data calculated by individual_analysis_csv. Once generated the csv files with the information-theoretic measures, the previous line should be commented to avoid repetition of calculations if you want to manipulate or reproduce figues.

    Data = np.array(Data.iloc[:, 1:]) #Remove first column of row labels.

    plot_individual_analysis(Data = Data, tradefreq = tradefreq, dfname = dfname, sec2av = sec2av, N = N, dx = dx, taux = taux, noise_ampl = noise_ampl, markersize = markersize) #Plot figures and save them in output_jpg
