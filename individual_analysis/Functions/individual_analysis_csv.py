import numpy as np
from ordpy import ordinal_sequence as ord_seq
from ordpy import permutation_entropy as perm_ent
from ordpy import ordinal_distribution as ord_dist
from ordpy import complexity_entropy as comp_ent
from ordpy import missing_patterns
from ordpy import renyi_entropy as renyi
from ordpy import ordinal_network as ord_net
from ordpy import random_ordinal_network as rand_ord_net
from ordpy import global_node_entropy as net_entropy
from ordpy import missing_links
import pandas as pd
from numba import njit, prange
from scipy.stats import entropy
from math import factorial as fac

@njit(parallel = True)
def avarray(meanprice, tradefreq, sec2av = 5): #This function averages the original dataframe in blocks of sec2av seconds. mean_price is the dataset of logarithms of prices, while tradefreq is the dataset of number of transactions per second of a given asset. 
    A = np.zeros((len(meanprice[:, 0]), len(range(0, len(meanprice[0, :]) - sec2av, sec2av))))
    for i in prange( len(meanprice[:, 0])):
        for j in range(len(range(0, len(meanprice[0, :]) - sec2av, sec2av))):
            if np.nansum(tradefreq[i, j*sec2av: (j + 1)*sec2av]) != 0:
                A[i, j] = np.nansum( meanprice[i, j*sec2av: (j + 1)*sec2av]*tradefreq[i, j*sec2av: (j+1)*sec2av] )/ np.nansum(tradefreq[i, j*sec2av: (j + 1)*sec2av]) 
            else:
                A[i, j] = np.nan
    return A 

def ind_pis(pis_sequence, pis_unique): #This function is defined to save time. When applying the function individual_analysis (see below) we will need to match certain couples of rrays (ordinal patterns), and this is more effectively accomplished by searching for their index in an absolute list, for which we need to match arrays just once. pis_sequence is an array of ordinal patterns and pis_unique is the absolute list of patterns from which we extract the index.
    ind = np.zeros(len(pis_sequence))
    for t in range(len(pis_sequence)):
        for i in range(len(pis_unique)):
            if np.all(pis_unique[i, :] == pis_sequence[t, :]):
                ind[t] = i
                break
    return ind

def freq_trans(n_pis, ord_indices, dx = 5): #This function measures the transition frequency of a sequence of ordinal patters, we will use it below in individual_analysis. n_pis is the number of patterns without repetition, ord_indices is the list of indices of a given sequence of ordinal patters (see ind_pis), dx is the dimension of ordinal patterns.
    freq_transition = np.zeros((n_pis, n_pis))
    freq = np.zeros(n_pis)
    for index in range(dx):
        non_overlapping_ind = ord_indices[index::dx] #ind_pis(ord_sequence[index::dx, :], pis_for_weights) #this is done to consider the dx posibilities of series of non-overlapping series of ordinal patterns
        for i1 in range(n_pis):
            for i2 in range(n_pis):
                freq_transition[i1, i2] += np.count_nonzero(np.array([non_overlapping_ind[t] == i1 and non_overlapping_ind[t+1] == i2 for t in range(len(non_overlapping_ind)-1)]))
            freq[i1] = np.nansum(freq_transition[i1, :])    
    return freq, freq_transition

def individual_analysis_csv(meanprice, tradefreq, dfname, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001): #this is the function which applies information-theoretic measures to individual stocks and saves them in a csv file in the ourput_csv folder. mean_price is the original data set of logarithms of mean prices per second, tradefreq is the dataset of numbers of transactions per second, sec2av is the number of seconds used when averaging with avarray function, N is the length in trading days of the subseries on which we calculate information-theoretic measures (which equeals 1 in the paper), dx is the dimension of ordinal patterns (which equals 5 in the paper), taux is a parameter of ordpy functions which we set equal to 1, noise_ampl is the amplitude of the uniform noise we add to our original subseries. 
    avdata = avarray(np.array(meanprice.iloc[:, 1: ]), np.array(tradefreq.iloc[:, 1: ]), sec2av)#the first column is removed, as it consist on row labels.
    avincrements = avdata[:, 1: ] - avdata[:, : -1] #We take the increments of sec2av-averaged logarithms of prices.
    L = avincrements.shape[0] - N #L is the number of subseries of N trading days.
    PE = np.zeros(L) #Permutation entropy
    freq_mis_trans = np.zeros(L) #Missing transitions frequency
    Com = np.zeros(L) #Statistical_Complexity
    Max_prob = np.zeros(L) #Maximum pattern probability
    global_node_ent = np.zeros(L) #Global node entropy
    min_node_ent = np.zeros(L)   #Minimum node entropy
    freq_mis_pat = np.zeros(L) #Missing patterns frequency
    for i in range(L):
        series = avincrements[i: i + N, :].flatten()
        series = series[~np.isnan(series)]
        np.random.seed(i)
        noise = noise_ampl*np.random.rand(len(series)) #
        series = series + noise
        PE[i], Com[i] = comp_ent(series, dx = dx, taux = taux)            
        _, freq_mis_pat[i] = missing_patterns(series, dx = dx, taux = taux)
        pis, rho = ord_dist(series, dx = dx, taux = taux, return_missing=True)
        Max_prob[i] = np.nanmax(rho)

        ord_sequence = ord_seq(series, dx = dx, taux = taux) 
        pis_for_weights, rho_for_weights = ord_dist(series, dx = dx, taux = taux, return_missing = False)       
        ord_indices = ind_pis(ord_sequence, pis_for_weights) #the index of pi in pis_for_weights for pi in ord_sequence
        n_pis = len(pis_for_weights)
        freq, freq_transition = freq_trans(n_pis, ord_indices, dx)
        prob_transition = np.zeros((len(pis_for_weights), len(pis_for_weights)))
        node_entropy = np.zeros(len(pis_for_weights))
                    
        for i1 in range(len(pis_for_weights)):
            for i2 in range(len(pis_for_weights)):
                prob_transition[i1, i2] = freq_transition[i1, i2]/freq[i1]     
            ind_no_zero = np.array([i2 for i2 in range(n_pis) if prob_transition[i1, i2] != 0])
            node_entropy[i1] = -np.sum(prob_transition[i1, ind_no_zero]*np.log(prob_transition[i1, ind_no_zero]))/np.log(fac(dx))
                                         
        freq_mis_trans[i] = np.nansum(prob_transition == 0)/(fac(dx)**2)    
    
        global_node_ent[i] = np.nansum(node_entropy*rho_for_weights) 
        min_node_ent[i] = np.nanmin(node_entropy)          
    cols = ['PE', 'freq_mis_pat', 'freq_mis_trans', 'Com', 'Max_prob', 'global_node_ent', 'min_node_ent'] 
    Data = np.zeros((L, len(cols)))       
    Data[:, 0] = PE
    Data[:, 1] = freq_mis_pat
    Data[:, 2] = freq_mis_trans 
    Data[:, 3] = Com
    Data[:, 4] = Max_prob
    Data[:, 5] = global_node_ent 
    Data[:, 6] = min_node_ent
                
                
                                
    pd.DataFrame(data = Data, columns = cols).to_csv('individual_analysis/output_csv/' + dfname + '_ord_measures_sec2av_{}_N{}_d{}_tau{}_noise{}.csv'.format(sec2av, N, dx, taux, noise_ampl))


