import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.collections import LineCollection
from matplotlib.colors import LinearSegmentedColormap
from matplotlib import cm
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
import os
import glob
from matplotlib import cm
import matplotlib 

def plot_individual_analysis(Data, tradefreq, dfname, sec2av = 5, N = 1, dx = 5, taux = 1, noise_ampl = 0.0000001, markersize = 0.3):
    PE = Data[:, 0]
    freq_mis_pat = Data[:, 1]
    freq_mis_trans = Data[:, 2] 
    Com = Data[:, 3]
    Max_prob = Data[:, 4]
    global_node_ent = Data[:, 5] 
    min_node_ent = Data[:, 6]

    tf = np.nansum(np.array(tradefreq.iloc[:, 1:]), axis = 1) #This is trade frequency: daily number of transactions.

                    
    fig, axs = plt.subplots(1, 3, figsize=(12, 4))
    axs[0].plot(PE, color = 'crimson', label = 'PE') #Left panel of the figure
    axs[0].plot(Com, color = 'black', label = 'Complexity')  
    #axs[0].plot(Max_prob, color = 'yellowgreen', label = 'Maximum Pattern Probability')                
    axs[0].plot(freq_mis_pat, label = 'Missing Patterns', color = 'orange') 
    axs[0].plot(freq_mis_trans, label = 'Missing Transitions', color = 'deeppink')   
    axs[0].plot(global_node_ent, color = 'sienna', label = 'GNE')  
    axs[0].plot(min_node_ent, color = 'steelblue', label = 'MNE')  
    axs[0].set_xlabel('Time')
    axs[0].legend(fontsize = 7, loc = (0.25,0.07))    
    axs[0].axvline(x = 82, linewidth = 0.5, color='gray', linestyle = '--')
    axs[0].axvline(x = 181, linewidth = 0.5, color='gray', linestyle = '--')
    axs[0].axvline(x = 201, linewidth = 0.5, color='gray', linestyle = '--')
    axs[0].set_title('(a)', x = 0.1, y = 0.85, fontsize = 10)

    dayslist = np.arange(len(min_node_ent))
    outlierlist = [82, 181, 201] #Outlier days
    #        colorlist = ['black' if s in outlierlist else 'indianred' for s in dayslist] #You can use this line to distinguish outlier from typical days, plotting outlier days in black in the central panel of the figure. In that case, comment the next line   
    colorlist = ['indianred' for s in dayslist]    
    axs[1].scatter(global_node_ent, min_node_ent, s = 5*markersize, c = colorlist)
    axs[1].plot(np.linspace(0,1,100), np.linspace(0,1,100), linewidth = 0.5, color='gray', linestyle = '--')
    axs[1].set_xlabel('Global Node Entropy')
    axs[1].set_ylabel('Minimum Node Entropy')
    axs[1].set_xlim([0,1])
    axs[1].set_ylim([0,1]) 
    axs[1].set_title('(b)', x = 0.1, y = 0.85, fontsize = 10)

    axs[2].plot(tf)   
    axs[2].set_xlabel('Time')
    axs[2].set_ylabel('Number of Transactions')
    axs[2].axvline(x = 82, linewidth = 0.5, color='gray', linestyle = '--')
    axs[2].axvline(x = 181, linewidth = 0.5, color='gray', linestyle = '--')
    axs[2].axvline(x = 201, linewidth = 0.5, color='gray', linestyle = '--')
    axs[2].set_title('(c)', x = 0.1, y = 0.85, fontsize = 10)
    #axs[2].legend()

    fig.set_dpi(300) #Figure resolution
                
            
    plt.subplots_adjust( wspace=0.35)#, hspace = 0.35)
    plt.tight_layout(pad = 1)        
    plt.savefig('individual_analysis/output_jpg/' + dfname + '_Ordplot_sec2av{}_N{}_d{}_tau{}_noise{}.jpg'.format(sec2av, N, dx, taux, noise_ampl))
    plt.close()
                

