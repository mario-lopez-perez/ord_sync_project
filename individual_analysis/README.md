This folder contains the code to reproduce the individual analysis of stocks carried out in section 4 of the paper. The file you have to compile to do this is main_individual.py; it is structured to follow the steps as indicated in the paper. There are three folders here: Functions, output_csv and output_jpg.

The folder functions contains two files: the first is called individual_analysis_csv.py and contains the functions used to process the data (calculate information-theoretic measures, tradinf frequency); the second is called individual_analysis_jpg.py and contains the functions used to plot that data as in the paper (dynamics of information-theoretic measures, MNE vs GNE plane, trading frequency).

The folder output_csv contains the data processed by some functions in individual_analysis_csv.py, data which is saved in csv files in order to avoid repetition of heavy calculations, such as GNE. In this way, once that data is at hand, the line in main_individual.py which carried out those calculations should be commented and the data accesed through the csv file with read_csv from pandas, as in main_individual.py.

The folder output_jpg contains the plots produced by the functions in individual_analysis_jpg.py. The names of those jpg files are set to be self-explanatory (as long as you read the paper and the code in main_individual.py), as they carry the information about parameters used and content.

Please note that the figures are not exactly the same as in the article, because at that stage this code was not structured and the random seeds used to generate noise when calculating ordinal-entropic measures were not standardized. Nevertheless, the results are notably similar, which is not surprise as ordinal patterns are known to be robust to noise and even noise-enhaced. 

